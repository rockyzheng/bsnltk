#!/usr/bin/env python
#-*-coding:utf-8-*-



from bsnltk.parse import keywords
keyword = ["北京", "北京天安门", "天安门", "长城", "北"]
trie = keywords.Trie()
trie.create_trie(keyword)
matchs = trie.parse_text("我在北京天安门看长城")
for m in matchs:
    print(m)


exit(0)

trie = keywords.Trie()
trie.add_keyword("北京")
trie.add_keyword("北京天安门")
trie.add_keyword("天安门")
trie.add_keyword("长城")
matchs = trie.parse_text("我在北京天安门看长城", False)
for m in matchs:
    print(m)
