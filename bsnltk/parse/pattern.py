#!/usr/bin/env python
# -*-coding:utf-8-*-

import re

EMAIL_PATTERN = re.compile("\w+@\w+\.\w+", re.ASCII)
PHONE_PATTERN = re.compile('(?:(132|130|131|134|135|136|137|138|139|147|148|150|151|152|157|155|156|153|183|'
                           '158|159|172|178|182|183|184|187|188|198|133|149|'
                           '153|173|174|177|180|181|189|199|170|176|186|132|185)\d{8})|(?:\d{3,4}-\d{8})', re.ASCII)

URL_PATTERN = re.compile("(https?|ftp|file)://[a-zA-z\d\.\-/#\?=&\+%]+", re.ASCII)


def parse_email(text, pattern=EMAIL_PATTERN):
    emails = []
    for m in re.finditer(pattern, text):
        emails.append((m.start(), m.end() + 1, m.group()))
    return emails


def parse_phone(text, pattern=PHONE_PATTERN):
    phones = []
    for m in re.finditer(pattern, text):
        phones.append((m.start(), m.end() + 1, m.group()))
    return phones


def parse_url(text, pattern=URL_PATTERN):
    urls = []
    for url in re.finditer(pattern, text):
        urls.append([url.start(), url.end() + 1, url.group()])
    return urls

