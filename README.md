# boss nlp tool kit

## Install

```
git clone https://gitee.com/rockyzheng/bsnltk.git 
cd bsnltk
python set_up.py install
```

## ac 多模匹配
 Aho-Corasick算法 Python 实现
example

```python
from bsnltk.parse import acmp
keywords = ["北京", "北京天安门", "天安门", "长城"]
trie = acmp.Trie()
trie.create_trie(keywords)
matchs = trie.parse_text("我在北京天安门看长城")
for m in matchs:
    print(m)
```

## todo 
ac 重叠区间去重
=======
# bsnltk
nlp langurage toolkit
