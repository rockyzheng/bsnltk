#!/usr/bin/env python
# -*-coding:utf-8-*-


from distutils.core import setup

setup(name='bsnltk',
    version='0.1',
    description='bs nlp language toolkit',
    author='wu.zheng',
    author_email='zhengwu314@163.com',
    url='',
    packages=['bsnltk', 'bsnltk.parse'],
    )
